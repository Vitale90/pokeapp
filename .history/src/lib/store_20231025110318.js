
import { writable } from 'svelte/store';

export const myStore = writable({
  pokemonName: '',
  pokemonAbilities: '',
  pokemonSprites: [],
  pokemonGame: [],
  pokemonMoves: [],
  pokemonStats: [],
  pokemonTypes:[],
});
	



