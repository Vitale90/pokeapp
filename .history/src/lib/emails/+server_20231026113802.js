import { json } from '@sveltejs/kit';
import { render } from 'svelte-email';
import Hello from '$lib/emails/Hello.svelte';
import nodemailer from 'nodemailer';
const transporter = nodemailer.createTransport({
	host: 'smtp.ethereal.email',
	port: 587,
	secure: false,
	auth: {
		user: 'dariomedugno@libero.it',
		pass: 'Aurora2021!'
	}
});

const emailHtml = render({
	component: Hello,
	props: {
		name: 'Svelte'
	}
});

const options = {
	from: 'dariomedugno@libero.it',
	to: 'coglione1@mailinator.com',
	subject: 'hello world',
	html: emailHtml
};

transporter.sendMail(options);