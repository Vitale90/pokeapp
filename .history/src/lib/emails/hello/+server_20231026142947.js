import { json } from '@sveltejs/kit';
import { render } from 'svelte-email';
import Hello from '$lib/emails/Hello.svelte';
import nodemailer from 'nodemailer';

const transporter = nodemailer.createTransport({
	host: 'smtp.ethereal.email',
	port: 587,
	secure: false,
	auth: {
		user: '	Braulio Stanton',
		pass: '	EEg92pDVqSkCmZqNak'
	}
});

const emailHtml = render({
	template: Hello,
	props: {
		name: 'Svelte'
	}
});

const options = {
	from: 'dario.medugno@dev.netengine.it',
	subject: 'Hello world',
	html: emailHtml
};

transporter.sendMail(options);