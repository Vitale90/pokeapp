import { render } from 'svelte-email';
import Hello from '$lib/emails/Hello.svelte';
import nodemailer from 'nodemailer';

const transporter = nodemailer.createTransport({
  host: 'smtp.ethereal.email',
  port: 587,
  secure: false,
  auth: {
    user: 'your_username', // Inserisci il tuo nome utente SMTP
    pass: 'your_password' // Inserisci la tua password SMTP
  }
});

async function sendEmail() {
  const emailHtml = render({
    template: Hello,
    props: {
      name: 'Svelte'
    }
  });

  const options = {
    from: 'your_email@example.com', // Indirizzo email del mittente
    to: 'recipient@example.com', // Indirizzo email del destinatario
    subject: 'Hello World',
    html: emailHtml
  };

  try {
    const info = await transporter.sendMail(options);
    console.log('Email sent:', info.messageId);
  } catch (error) {
    console.error('Errore nell\'invio dell\'email:', error);
  }
}

sendEmail();