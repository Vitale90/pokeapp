import { render } from 'svelte-email';
import Hello from '$lib/emails/Hello.svelte';
import nodemailer from 'nodemailer';

const transporter = nodemailer.createTransport({
  host: 'smtp.ethereal.email',
  port: 587,
  secure: false,
  auth: {
    user: 'braulio.stanton13@ethereal.email', // Inserisci il tuo nome utente SMTP
    pass: 'EEg92pDVqSkCmZqNak' // Inserisci la tua password SMTP
  }
});

async function sendEmail() {
  const emailHtml = render({
    template: Hello,
    props: {
      name: 'Svelte'
    }
  });

  const options = {
    from: 'braulio.stanton13@ethereal.email', // Indirizzo email del mittente
    to: 'dario.medugno@dev.netengine.it', // Indirizzo email del destinatario
    subject: 'Hello World',
    html: emailHtml
  };

  try {
    const info = await transporter.sendMail(options);
    console.log('Email sent:', info.messageId);
  } catch (error) {
    console.error('Errore nell\'invio dell\'email:', error);
  }
}

sendEmail();