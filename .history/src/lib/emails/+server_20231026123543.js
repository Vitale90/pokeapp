import { json } from '@sveltejs/kit';
import { render } from 'svelte-email';
import Hello from 'routes/lib/emails/Hello.svelte';
import nodemailer from 'nodemailer';


const transporter = nodemailer.createTransport({
    host: 'sandbox.smtp.mailtrap.io', 
    port: 2525, 
    secure: false, 
    auth: {
      user: 'b75ccae2168fe5', 
    }
  });

export default async function handle({ request }) {
  // Get the form data
  const formData = await request.formData();

  // Send the email
  const emailHtml = render({
    component: Hello,
    props: {
      name: formData.get('to')
    }
  });

  const options = {
    from: 'dariomedugno@libero.it',
    to: formData.get('to'),
    subject: formData.get('subject'),
    html: emailHtml
  };

  try {
    await transporter.sendMail(options);

    // Return a JSON response indicating success
    return json({ success: true });
  } catch (error) {
    // Return a JSON response indicating failure
    return json({ error: error.message });
  }
}