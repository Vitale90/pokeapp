
import { writable } from 'svelte/store';

export const myStore = writable({
  pokemonName: '',
  pokemonAbilities: '',
  pokemonSprites: [],
  pokemonMoves: [],
  pokemonStats: [],
  pokemonTypes:[],
});
	

export const page = writable(1);
export const pageSize = 20;
export const pokemonData = writable([]);



