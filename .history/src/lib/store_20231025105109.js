
import { writable } from 'svelte/store';

export const myStore = writable({
  content: '',
  pokemonName: '',
  pokemonAbilities: '',
  pokemonSprites: [],
  pokemonGame: [],
  pokemonMoves: [],
  pokemonStats: [],
  pokemonTypes:[],
});
	



