
import { writable } from 'svelte/store';

export const myStore = writable({
  content: '',
  pokemonName: '',
  pokemonAbilities: '',
  pokemonSprites: [],
  pokemonGame: [],
  pokemonMoves: [],
  pokemonStats: [],
  pokemonTypes:[],
});

const getContent = async () => {
	const res = await fetch(`https://pokeapi.co/api/v2/pokemon/${content}`);
	const contentJson = await res.json();
	console.log(contentJson);
	
	myStore.update((storeData) => {
	  storeData.pokemonName = contentJson.name;
	  storeData.pokemonAbilities = contentJson.abilities;
	  storeData.pokemonGame = contentJson.game_indices;
	  storeData.pokemonMoves = contentJson.moves;
	  storeData.pokemonStats = contentJson.stats;
	  storeData.pokemonTypes = contentJson.types;
	  return storeData; // Return the updated data
	});
  };

