
import { writable } from 'svelte/store';

export const myStore = writable({
  pokemonName: '',
  pokemonAbilities: '',
  pokemonSprites: [],
  pokemonMoves: [],
  pokemonStats: [],
  pokemonTypes:[],
});
	
export const paginationStore = writable({
  page: 0,
  pageSize: 20,
  pokemonData: [],
})




