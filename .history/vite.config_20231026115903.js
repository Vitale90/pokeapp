import { sveltekit } from '@sveltejs/kit/vite';
import { defineConfig } from 'vite';
import { createServer } from 'vite'

export default defineConfig({
	plugins: [sveltekit()]
});



export default {
  server: {
    fs: {
      root: 'src'
    }
  }
};
