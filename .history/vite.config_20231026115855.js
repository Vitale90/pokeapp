import { sveltekit } from '@sveltejs/kit/vite';
import { defineConfig } from 'vite';

export default defineConfig({
	plugins: [sveltekit()]
});

import { createServer } from 'vite';

export default {
  server: {
    fs: {
      root: 'src'
    }
  }
};
