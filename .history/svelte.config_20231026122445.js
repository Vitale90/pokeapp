import adapter from '@sveltejs/adapter-auto';

/** @type {import('@sveltejs/kit').Config} */
const config = {
	kit: {
		// adapter-auto only supports some environments, see https://kit.sveltejs.dev/docs/adapter-auto for a list.
		// If your environment is not supported or you settled on a specific environment, switch out the adapter.
		// See https://kit.sveltejs.dev/docs/adapters for more information about adapters.
		adapter: adapter()
	},
	routes: [
		{
		  path: '/emails/',
		  method: 'post',
		  handler: '/emails/+server.js'
		}
	]
};

export default config;
