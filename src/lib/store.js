import { writable } from 'svelte/store';

export const myStore = writable({
  pokemonName: '',
  pokemonAbilities: '',
  pokemonSprites: [],
  pokemonMoves: [],
  pokemonStats: [],
  pokemonTypes:[],
});
	
export const paginationStore = writable({
  page: 1,
  pageSize: 20,
  pokemonData: [],
})




